# Twilight Swaywm

My dotfiles for swaywm.

## Dependencies

* sway (duh)
* waybar
* rofi
* termite
* mako (notifications)
* swaylock-fancy
* swayidle (optional, lock screen after suspend)
* ttf-awesome-font (for waybar icons)

## Some extra keybindings 
- $mod+b Hide/Show waybar
- $mod+F1 Firefox
- $mod+F2 Ranger
- $mod+F3 Emacas
- $mod+F4 Telegram
- In "resize" mode, shift + arrows to faster resize

## Screenshots
![alt text](screenshots/locked.png "Locked")
![alt text](screenshots/unlocked.png "Unlocked")