# Materia blue dotfiles

#Reset waybar and idle
exec killall waybar && killall swayidle 

# Variables
set $mod Mod4
set $term termite #terminal
set $menu rofi -show drun | xargs swaymsg exec #rofi
#Set keyboard
set $lan us_intl
set $lan_variant extd

#Mako notifications
set $mako_options --background-color "#222222" --border-color "#2196F3" --width 400 --height 800 --max-visible 2 --default-timeout 3500

# Run pass and rofi to quickly access passwords
set $password ~/.local/bin/gopass ls -f | rofi -dmenu | xargs -i ~/.local/bin/gopass -c {}

#Wallpaper
output * bg ~/Pictures/background.jpg stretch


#Lock screen
exec swayidle -w before-sleep 'swaylock-fancy -p -f Consolas-Bold' #lock and suspend 
bindsym $mod+l exec systemctl suspend
bindsym $mod+Shift+l systemclt hibernate

#Border and gaps
default_border pixel 2
default_floating_border pixel
gaps outer 2
gaps inner 8

# Float some windows by default
for_window [app_id="nm-connection-editor"] floating enable

input "*" {
   
    xkb_layout $lan
#    xkb_variant $lan_variant

}
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

#Shortcuts

#Reload configuration
bindsym $mod+Shift+r reload

#Show/Hide waybar
bindsym $mod+b exec killall waybar || waybar

# start a terminal
bindsym $mod+Return exec $term

# kill focused window
bindsym $mod+Shift+q kill

# start your launcher
bindsym $mod+d exec $menu

#Firefox, Ranger and Emacs
bindsym $mod+F1 exec firefox
bindsym $mod+F2 exec termite -e ranger
bindsym $mod+F3 exec emacs
bindsym $mod+F4 exec ./tsetup*/Telegram/Telegram

#volume
bindsym XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5% && aplay ~/.config/sway/audio-volume-change.wav
bindsym XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5% && aplay ~/.config/sway/audio-volume-change.wav
bindsym XF86AudioMicMute exec pactl set-source-mute @DEFAULT_SOURCE@ toggle

#screen brightness
bindsym XF86MonBrightnessUp exec light -A 5
bindsym XF86MonBrightnessDown exec light -U 5

#Screenshot
bindsym Print exec grim ~/Pictures/$(date +'%Y-%m-%d-%H-%M-%S.png') && aplay ~/.config/sway/camera-shutter.wav
bindsym Shift+Print exec grim -g "$(slurp)" ~/Pictures/'%Y-%m-%d-%H%M%S_selection.png'

# Drag floating windows by holding down $mod and left mouse button.
# Resize them with right mouse button + $mod.
# Despite the name, also works for non-floating windows.
# Change normal to inverse to use left mouse button for resizing and right
# mouse button for dragging.
floating_modifier $mod normal

# reload the configuration file
bindsym $mod+Shift+c reload

# exit sway (logs you out of your Wayland session)
bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway?' -b 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move your focus around
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right



#Workspaces and containers

focus_follows_mouse no
 
#Switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6

bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

#Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

#Change layout split
bindsym $mod+h splith
bindsym $mod+v splitv

#Switch to stacked,tabbed or split layout
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Make the current focus fullscreen
bindsym $mod+f fullscreen

#Toggle the current focus between tiling and floating mode
bindsym $mod+Shift+space floating toggle

#Switch focus between the tiling area and the floating area
bindsym $mod+space focus mode_toggle

# move focus to the parent container
bindsym $mod+a focus parent

#Move focused window to scratchpad
bindsym $mod+Shift+minus move scratchpad

#Cycles through scratchpad windows.
bindsym $mod+minus scratchpad show

#Resize containers
mode "resize" {

    # ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    bindsym Shift+Left resize shrink width 40px
    bindsym Shift+Down resize grow height 40px
    bindsym Shift+Up resize shrink height 40px
    bindsym Shift+Right resize grow width 40px
        
    
    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

bindsym $mod+n mode "notifications"
mode "notifications" {
	bindsym Escape mode "default"
	bindsym Return exec makoctl invoke; exec makoctl dismiss; mode "default"
	bindsym d exec makoctl dismiss; mode "default"
	bindsym Shift+d exec makoctl dismiss -a; mode "default"

}

seat "*" {
    hide_cursor 10000
}

#Color and style

#class                 border  backgr. text    indicator   child_border
client.focused          #2196F3 #383c4a #ffffff #2196F3     #2196F3 		
client.focused_inactive #F5F5F5 #383c4a #ffffff #404040     #888888
client.unfocused        #888888 #222222 #888888 #404040     #888888
client.urgent           #900000 #900000 #ffffff #900000     #900000
client.placeholder      #0c0c0c #0c0c0c #ffffff #404040     #404040


for_window [title=".*Telegram$"] floating enable
for_window [title=".*Spotify$"] floating enable


#Exec services

exec mako $mako_options

exec redshift

exec bash .config/waybar/waybar.sh

exec waybar
